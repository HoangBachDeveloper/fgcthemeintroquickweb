$(function () {
    // MAIN BODY SLIDE
    $('.main-body__slide__wraps').slick({

        // prevArrow: $('#next-slide'),
        arrows: false
    });

    // MAKE DROPDOWN BUTTON MOVE UP AND DOWN
    setInterval(() => {
        $('.slide-dropdown__inner img').toggleClass('active');
    }, 300)

    // CLICK ON DROPDOWN BUTTON 
    $('.slide-dropdown__inner img').click(function (e) {
        e.preventDefault();
        $(window).scrollTop(785);
    });



    // QUOTE SLIDE IN HOME
    $('.main-body__quote-wraps').slick({
        slidesToShow: 2,
        arrows: false,
        dots: true,
        slidesToScroll: 2,
        autoplay: true,
        autoplaySpeed: 3000,
        responsive: [
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                }
            }
        ]
    });

    // CHECKBOX IN TEMPLATE PAGES
    $('.checkbox__wraps').click(function (e) {
        $(this).children('span').toggleClass('active');
    });

    // RANGER SLIDER IN TEMPLATE PAGE
    $('.template-price').ionRangeSlider(
        {
            type: "double",
            min: 0,
            max: 1000,
            from: 200,
            to: 500,
            grid: false,
            skin: "round",
            prefix: "$"
        }
    );

    // BLOG SLIDE
    $('.blog-body__slide').slick({
        arrows: false,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,

    });

    // INVOLVE SLIDE IN TEMPLATE DETAILS
    $('.invonve__slide').slick({
        slidesToScroll: 2,
        dots: false,
        arrows: false,
        slidesToShow: 2,
        autoplay: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                }
            }
        ]
    });

    // OPEN HEADER ON SMALL DISPLAY
    $('#header-menu__open').click(function (e) {
        $('.menu-inner__small').addClass('active');
    })

    // CLOSE HEADER ON SMALL DISPLAY
    $('#menu-inner__small__close').click(function (e) {
        $('.menu-inner__small').removeClass('active');
    })
});