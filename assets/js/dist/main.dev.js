"use strict";

$(function () {
  // MAIN BODY SLIDE  
  $('.main-body__slide-wraps').slick({
    vertical: true,
    verticalSwiping: true,
    prevArrow: $('#next-slide'),
    nextArrow: false
  }); // MAKE PREVARROW BUTTON MOVE UP AND DOWN

  setInterval(function () {
    $('.body-slide__button').toggleClass('active');
  }, 300);
  $('.main-body__quote-wraps').slick({
    slidesToShow: 2,
    arrows: false,
    dots: true,
    slidesToScroll: 2,
    autoplay: true,
    autoplaySpeed: 3000
  }); // CHECKBOX IN TEMPLATE PAGES

  $('.checkbox__wraps').click(function (e) {
    $(this).children('span').toggleClass('active');
  }); // RANGER SLIDER IN TEMPLATE PAGE

  $('.template-price').ionRangeSlider({
    type: "double",
    min: 0,
    max: 1000,
    from: 200,
    to: 500,
    grid: false,
    skin: "round",
    prefix: "$"
  });
});